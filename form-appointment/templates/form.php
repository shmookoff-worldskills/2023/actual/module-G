<form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
    <input type="hidden" name="action" value="book_appointment">
    <label for="date">Date:</label>
    <input type="date" id="date" name="date" required>
    <br>
    <label for="time">Time:</label>
    <input type="time" id="time" name="time" required>
    <br>
    <label for="first_name">First Name:</label>
    <input type="text" id="first_name" name="first_name" required>
    <br>
    <label for="middle_name">Middle Name:</label>
    <input type="text" id="middle_name" name="middle_name">
    <br>
    <label for="last_name">Last Name:</label>
    <input type="text" id="last_name" name="last_name" required>
    <br>
    <label for="expert">Expert:</label>
    <select id="expert" name="expert" required>
        <option value="">Select an expert</option>
        <option value="doctor1">Doctor 1</option>
        <option value="doctor2">Doctor 2</option>
        <option value="doctor3">Doctor 3</option>
    </select>
    <br>
    <input type="submit" value="Book Appointment">
</form>