<?php
/*
 * Plugin Name:       Form Appointment
 * Description:       Creates a shortcode with form for appointment booking
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            shmookoff
 * Author URI:        https://gitlab.com/shmookoff
 * Text Domain:       form-appointment
 * Domain Path:       /languages
 */

function create_appointments_table() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'appointments';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        date date NOT NULL,
        time time NOT NULL,
        first_name varchar(50) NOT NULL,
        middle_name varchar(50),
        last_name varchar(50) NOT NULL,
        expert varchar(50) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
register_activation_hook( __FILE__, 'create_appointments_table' );

function drop_appointments_table() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'appointments';
    
    $sql = "DROP TABLE $table_name;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
register_deactivation_hook(__FILE__, 'drop_appointments_table');

function appointment_form_shortcode() {
    ob_start();
    include( plugin_dir_path( __FILE__ ) . 'templates/form.php' );
    return ob_get_clean();
}
add_shortcode( 'appointment_form', 'appointment_form_shortcode' );

function is_expert_available($date, $time, $expert) {
    global $wpdb;

    $table_name = $wpdb->prefix . 'appointments';

    $query = "SELECT COUNT(*) FROM $table_name WHERE date = %s AND time = %s AND expert = %s";

    $count = $wpdb->get_var($wpdb->prepare($query, $date, $time, $expert));

    return $count === '0';
}

function book_appointment() {
    global $wpdb;

    if (isset($_POST['date']) && isset($_POST['time']) && isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['expert'])) {
        $date = sanitize_text_field($_POST['date']);
        $time = sanitize_text_field($_POST['time']);
        $first_name = sanitize_text_field($_POST['first_name']);
        $middle_name = sanitize_text_field($_POST['middle_name']);
        $last_name = sanitize_text_field($_POST['last_name']);
        $expert = sanitize_text_field($_POST['expert']);

        $table_name = $wpdb->prefix . 'appointments';

        // Check if expert is available
        if (is_expert_available($date, $time, $expert)) {
    
            // Insert appointment into database
            $data = array(
                'date' => $date,
                'time' => $time,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'last_name' => $last_name,
                'expert' => $expert
            );
    
            $format = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            );
    
            $wpdb->insert($table_name, $data, $format);
    
            // Display confirmation message
            echo 'Appointment booked successfully!';
        } else {
            // Display error message
            echo 'Sorry, the selected expert is not available at this time. Please choose a different time or expert.';
        }
    }
}
add_action( 'admin_post_book_appointment', 'book_appointment' );