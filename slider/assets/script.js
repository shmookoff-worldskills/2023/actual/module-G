document.addEventListener('DOMContentLoaded', function() {
    // Define slider variables
    const slider = document.getElementById('slider');
    const slides = slider.querySelectorAll('.slide');
    const navArrows = slider.querySelectorAll('.nav-arrow');
    const slideInfo = slider.querySelector('.slide-info');
    const slideCount = slides.length;

    let activeIndex = 0;
    let autoplayInterval = null;

    // Set up initial slide state
    slides[0].classList.add('active');
    slideInfo.textContent = 'Slide 1 of ' + slideCount;

    // Define function to handle slide changes
    function changeSlide(index) {
        // Update active slide
        slides[activeIndex].classList.remove('active');
        slides[index].classList.add('active');

        // Update slide info
        slideInfo.textContent = 'Slide ' + (index + 1) + ' of ' + slideCount;
        activeIndex = index;
    }

    // Define function to handle autoplay
    function autoplay() {
        const nextIndex = (activeIndex + 1) % slideCount;
        changeSlide(nextIndex);
    }

    // Set up autoplay
    autoplayInterval = setInterval(autoplay, 5000);

    // Add click handlers to navigation arrows
    for (let i = 0; i < navArrows.length; i++) {
        navArrows[i].addEventListener('click', function() {
            // Clear autoplay interval
            clearInterval(autoplayInterval);

            // Determine next index
            let index;
            if (this.classList.contains('prev')) {
                index = (activeIndex - 1 + slideCount) % slideCount;
            } else {
                index = (activeIndex + 1) % slideCount;
            }

            // Change slide
            changeSlide(index);

            // Restart autoplay
            autoplayInterval = setInterval(autoplay, 5000);
        });
    }
});
