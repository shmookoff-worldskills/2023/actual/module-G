<?php
/*
 * Plugin Name:       Slider
 * Description:       Creates a shortcode with slider
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            shmookoff
 * Author URI:        https://gitlab.com/shmookoff
 * Text Domain:       slider
 * Domain Path:       /languages
 */

function slider_shortcode() {
    // Generate HTML markup for slider
    $slider_html = '<div id="slider" class="slider">';
    
    $slider_html .= '<div class="slide">1 slide</div>';
    $slider_html .= '<div class="slide">2 slide</div>';
    $slider_html .= '<div class="slide">3 slide</div>';
    $slider_html .= '<div class="slide">4 slide</div>';
    
    $slider_html .= '<div class="slide-info"></div>';
    $slider_html .= '<div class="nav-arrow prev">Prev</div>';
    $slider_html .= '<div class="nav-arrow next">Next</div>';
    
    $slider_html .= '</div>';

    return $slider_html;
}
add_shortcode( 'slider', 'slider_shortcode' );

function slider_init() {
    wp_enqueue_style( 'slider-style', plugins_url( 'slider/assets/styles.css' ) );
    wp_enqueue_script( 'slider-script', plugins_url( 'slider/assets/script.js' ));
}

add_action( 'wp_enqueue_scripts', 'slider_init' );